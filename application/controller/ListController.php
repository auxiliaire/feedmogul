<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 18:15
 */

class ListController extends Mogul\DefaultController
{
    public function indexAction()
    {
        $twitter = new \Mogul\Social\Twitter($this->db);

        $mogul = $this->db->findOneBy("SocialProfile", "socialProfileScreenName", TWITTER_USER);

        $twitter->setUser($mogul);

        $this->view->user = $twitter->getUser();
        $this->view->backgroundColor = $twitter->getUser()->getSocialProfileBackgroundColor();
        $this->view->linkColor = $twitter->getUser()->getSocialProfileLinkColor();
        $this->view->textColor = $twitter->getUser()->getSocialProfileTextColor();
        $this->view->statuses = $twitter->fetch();
    }

    public function retweetAction()
    {
        $twitter = new \Mogul\Social\Twitter($this->db);

        if (isset($_REQUEST['id'])) {
            $_SESSION['retweet_id'] = $_REQUEST['id'];
        }

        if (!isset($_SESSION['access_token'])) {
            $this->_userAuth($twitter, "/retweet");
        }
        $access_token = $_SESSION['access_token'];
        $twitter->connect()->setOauthToken($access_token['oauth_token'], $access_token['oauth_token_secret']);

        $this->view->response = $twitter->connect()->post("statuses/retweet", array("id" => $_SESSION['retweet_id']));
    }

    public function favoriteAction()
    {
        $twitter = new \Mogul\Social\Twitter($this->db);

        if (isset($_REQUEST['id'])) {
            $_SESSION['favorite_id'] = $_REQUEST['id'];
        }

        if (!isset($_SESSION['access_token'])) {
            $this->_userAuth($twitter, "/favorite");
        }
        $access_token = $_SESSION['access_token'];
        $twitter->connect()->setOauthToken($access_token['oauth_token'], $access_token['oauth_token_secret']);

        $this->view->response = $twitter->connect()->post("favorites/create", array("id" => $_SESSION['favorite_id']));
    }

    public function replyAction()
    {
        $twitter = new \Mogul\Social\Twitter($this->db);

        if (isset($_REQUEST['id'])) {
            $_SESSION['reply_id'] = $_REQUEST['id'];
        }

        if (!isset($_SESSION['access_token'])) {
            $this->_userAuth($twitter, "/reply");
        }
        $access_token = $_SESSION['access_token'];
        $twitter->connect()->setOauthToken($access_token['oauth_token'], $access_token['oauth_token_secret']);

        if (isset($_POST['text'])) {
            // TODO: Validate input
            $this->view->response = $twitter->connect()->post("statuses/update", array(
                "status" => $_POST['text'],
                "in_reply_to_status_id" => $_SESSION['reply_id']
            ));
        } else {
            /* @var $status Mogul\Model\SocialStatus */
            $status = $this->db->findOneBy("SocialStatus", "publicSocialStatusId", $_SESSION['reply_id']);
            if ($status) {
                /* @var $profile Mogul\Model\SocialProfile */
                $profile = $this->db->find("SocialProfile", $status->getSocialProfileId());
                $this->view->text = "@" . $profile->getSocialProfileScreenName() . ' ';
            } else {
                $this->view->text = "ERROR: status not found.";
            }
        }
    }

    protected function _userAuth($twitter, $redirect) {
        if (!isset($_REQUEST['oauth_token'])) {
            $request_token = $twitter->connect()->oauth('oauth/request_token', array('oauth_callback' => "http://" . $_SERVER['HTTP_HOST'] . $redirect));

            $_SESSION['oauth_token'] = $request_token['oauth_token'];
            $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

            $url = $twitter->connect()->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));

            header("Location: $url");
            exit;
        } else {
            $request_token = array();
            $request_token['oauth_token'] = $_SESSION['oauth_token'];
            $request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];

            if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
                throw new \Exception("Twitter authentication error (token mismatch: '{$request_token['oauth_token']}' != '{$_REQUEST['oauth_token']}').");
            }
            /* @var $connection \Abraham\TwitterOAuth\TwitterOAuth */
            $connection = $twitter->connect();
            $connection->setOauthToken($request_token['oauth_token'], $request_token['oauth_token_secret']);
            $access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));

            $_SESSION['access_token'] = $access_token;

            /* @var $user \Mogul\Model\SocialProfile */
            $user = $this->db->findOneBy("SocialProfile", "publicSocialProfileId", $access_token['user_id']);
            if ($user->isNew()) {
                $connection->setOauthToken($access_token['oauth_token'], $access_token['oauth_token_secret']);
                $twitterUser = $connection->get("account/verify_credentials");
                $user->setSocialSettingId($twitter->getSocialSetting()->getSocialSettingId());
                $user->setSocialProfileName($twitterUser->name);
                $user->setSocialProfileScreenName($twitterUser->screen_name);
                $user->setSocialProfileImage($twitterUser->profile_image_url);
                // Skip saving sensitive data in demo
                //$user->setSocialProfileOauthToken($access_token['oauth_token']);
                //$user->setSocialProfileOauthTokenSecret($access_token['oauth_token_secret']);
                $user->store();
            }
        }
    }

}