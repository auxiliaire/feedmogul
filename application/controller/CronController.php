<?php

/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 21:36
 */

class CronController extends \Mogul\DefaultController
{
    public function fetchAction()
    {
        $twitter = new \Mogul\Social\Twitter($this->db);

        $mogul = $this->db->findOneBy("SocialProfile", "socialProfileScreenName", TWITTER_USER);

        $socialProfile = $twitter->setUser($mogul)->getUser();
        foreach ($twitter->update()->getStatuses() as $status) {
            $d = new DateTime($status->created_at);

            $socialStatus = new \Mogul\Model\SocialStatus($this->db);
            $socialStatus->setSocialProfileId($socialProfile->getSocialProfileId());
            $socialStatus->setPublicSocialStatusId($status->id_str);
            $socialStatus->setSocialStatusCreatedAt($d->format("Y-m-d H:i:s"));
            $socialStatus->setSocialStatusText($status->text);
            $socialStatus->store();

            $this->_printStatus($status, $d);
        }
        exit;
    }

    protected function _printStatus($status, DateTime $d)
    {
        echo $status->user->name . ' @' . $status->user->screen_name . ' ' . $d->format("j M Y") . PHP_EOL;
        echo $status->text . PHP_EOL;
        echo "------------------------------------------------------------------------------------------" . PHP_EOL;
    }
}