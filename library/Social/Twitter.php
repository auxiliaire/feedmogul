<?php

namespace Mogul\Social;

use Abraham\TwitterOAuth\TwitterOAuth;
use Mogul\Model\DB;

class Twitter implements ISocial
{
    const STRING_ID = "twitter";
    const DK_CONSUMER_KEY    = "consumer_key";
    const DK_CONSUMER_SECRET = "consumer_secret";
    const DK_TOKEN           = "token";
    const DK_TOKEN_SECRET    = "token_secret";

    /**
     * @var \Mogul\Model\DB
     */
    protected $db = NULL;
    protected $_settings = NULL;
    /**
     * @var TwitterOAuth
     */
    protected $_connection = NULL;
    protected $_statuses = NULL;
    /**
     * @var \Mogul\Model\SocialSetting
     */
    protected $_socialSetting = NULL;
    /**
     * @var \Mogul\Model\SocialProfile
     */
    protected $_user = NULL;

    public function __construct(DB $db)
    {
        $this->db = $db;
        $this->_initSettings();

        $this->connect();
        //$content = $this->_connection->get("account/verify_credentials");
        //var_dump($content);
    }

    protected function _initSettings()
    {
        $this->_setSocialSetting($this->db->findOneBy("SocialSetting", "socialSettingStringId", self::STRING_ID));
        $stmt = $this->db->getConnection()->prepare("
            SELECT d.* FROM socialSettingData d
            WHERE d.socialSettingId = :id
        ");
        $stmt->execute(array("id" => $this->getSocialSetting()->getSocialSettingId()));
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // Normally I would store this in an object not in an array:
        $this->_settings = array();
        foreach ($result as $row) {
            $this->_settings[$row['socialSettingDataKey']] = $row['socialSettingDataValue'];
        }
    }

    /**
     * @return TwitterOAuth
     */
    public function connect() {
        if (NULL === $this->_connection) {
            $this->_connection = new TwitterOAuth(
                $this->getConsumerKey(),
                $this->getConsumerSecret(),
                $this->getAccessToken(),
                $this->getAccessTokenSecret()
            );
        }
        return $this->_connection;
    }

    public function save() {

    }

    /**
     * Updates the statuses from Twitter.
     * @return $this
     */
    public function update() {
        $this->_statuses = $this->connect()->get("statuses/user_timeline", array("user_id" => (int)$this->getUser()->getPublicSocialProfileId()));
        return $this;
    }

    /**
     * Fetches the statuses from the DB.
     * return array
     */
    public function fetch()
    {
        $stmt = $this->db->getConnection()->prepare("
            SELECT s.* FROM socialStatus s
            WHERE s.socialProfileId = :id
        ");
        $stmt->execute(array("id" => $this->getUser()->getSocialProfileId()));

        $result = $stmt->fetchAll(\PDO::FETCH_CLASS, "Mogul\Model\SocialStatus");
        return $result;
    }

    public function getStatuses() {
        return $this->_statuses;
    }

    public function storeStatuses() {

    }

    public function comment() {

    }

    public function share() {

    }

    public function like() {

    }

    public function getStringId()
    {
        return self::STRING_ID;
    }

    public function getConsumerKey()
    {
        return $this->_settings[self::DK_CONSUMER_KEY];
    }

    public function getConsumerSecret()
    {
        return $this->_settings[self::DK_CONSUMER_SECRET];
    }

    public function getAccessToken()
    {
        return $this->_settings[self::DK_TOKEN];
    }

    public function getAccessTokenSecret()
    {
        return $this->_settings[self::DK_TOKEN_SECRET];
    }

    /**
     * @return \Mogul\Model\SocialProfile
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @param int|string|\Mogul\Model\SocialProfile $user
     */
    public function setUser($user)
    {
        if (is_a($user, "Mogul\Model\SocialProfile")) {
            /* @var $user \Mogul\Model\SocialProfile */
            if ($user->isNew()) {
                $idKey = "screen_name";
                $value = $user->getSocialProfileScreenName();
            } else {
                $this->_user = $user;
                return $this;
            }
        } else if (is_int($user)) {
            $idKey = "user_id";
            $value = $user;
            $user = new \Mogul\Model\SocialProfile($this->db);
        } else {
            $idKey = "screen_name";
            $value = $user;
            $user = new \Mogul\Model\SocialProfile($this->db);
        }
        // No user found in DB, fetching user data:
        $twitterUser = $this->connect()->get("users/show", array($idKey => $value));

        $user->setSocialSettingId($this->getSocialSetting()->getSocialSettingId());
        $user->setPublicSocialProfileId($twitterUser->id_str);
        $user->setSocialProfileImage($twitterUser->profile_image_url);
        $user->setSocialProfileName($twitterUser->name);
        $user->setSocialProfileScreenName($twitterUser->screen_name);
        $user->setSocialProfileBackgroundColor($twitterUser->profile_background_color);
        $user->setSocialProfileLinkColor($twitterUser->profile_link_color);
        $user->setSocialProfileTextColor($twitterUser->profile_text_color);

        // Updating database:
        $user->store();

        $this->_user = $user;
        return $this;
    }

    /**
     * @return \Mogul\Model\SocialSetting
     */
    public function getSocialSetting()
    {
        return $this->_socialSetting;
    }

    /**
     * @param \Mogul\Model\SocialSetting $socialSetting
     */
    protected function _setSocialSetting($socialSetting)
    {
        $this->_socialSetting = $socialSetting;
    }

}