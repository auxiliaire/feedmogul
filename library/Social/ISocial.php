<?php

namespace Mogul\Social;

interface ISocial
{
    public function connect();
    public function save();
    public function update();
    public function getStatuses();
    public function storeStatuses();
    public function comment();
    public function share();
    public function like();
    public function getStringId();
}