<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 18:17
 */

namespace Mogul;

class DefaultController
{
    public $view = NULL;
    /**
     * @var null|\PDO
     */
    public $db = NULL;

    public function __construct()
    {
        $this->view = new View();
        $this->db = \Mogul\Model\DB::getInstance();
    }

    public function __call($name, $args)
    {
        $actionName = $name . 'Action';
        if (method_exists($this, $actionName)) {
            call_user_method_array($actionName, $this, $args);
            $script = VIEW_PATH . '/' . strtolower(str_replace("Controller", '', get_class($this))) . '/' . $name . '.phtml';
            if (!file_exists(realpath($script))) {
                throw new \Exception("View script ({$script}) not found!");
            }
            $this->view->setScript($script);
            return $this->view->render();
        } else {
            throw new \Exception("Method ($actionName) not found in class (" . get_class($this) . ")!");
        }
    }
}