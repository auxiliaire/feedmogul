<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 19:09
 */

namespace Mogul;

class View
{
    protected $_script = NULL;

    /**
     * @return null
     */
    public function getScript()
    {
        return $this->_script;
    }

    /**
     * @param null $script
     */
    public function setScript($script)
    {
        $this->_script = $script;
    }

    public function __set($key, $val) {
        $this->$key = $val;
        return;
    }

    public function __get($key) {
        return null;
    }

    public function __isset($key) {
        return isset($this->$key);
    }

    public function render()
    {
        ob_start();

        include $this->_script;

        return ob_get_clean();
    }

}