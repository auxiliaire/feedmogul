<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.27.
 * Time: 11:54
 */

namespace Mogul\Model;

class SocialProfile extends AStorable
{
    public $socialProfileId = NULL;
    public $publicSocialProfileId = NULL;
    public $socialSettingId = NULL;
    public $socialProfileName = NULL;
    public $socialProfileScreenName = NULL;
    public $socialProfileImage = NULL;
    public $socialProfileOauthToken = NULL;
    public $socialProfileOauthTokenSecret = NULL;
    public $socialProfileBackgroundColor = NULL;
    public $socialProfileLinkColor = NULL;
    public $socialProfileTextColor = NULL;

    /**
     * @return null
     */
    public function getSocialProfileId()
    {
        return $this->socialProfileId;
    }

    /**
     * @param null $socialProfileId
     */
    public function setSocialProfileId($socialProfileId)
    {
        $this->socialProfileId = $socialProfileId;
    }

    /**
     * @return null
     */
    public function getPublicSocialProfileId()
    {
        return $this->publicSocialProfileId;
    }

    /**
     * @param null $publicSocialProfileId
     */
    public function setPublicSocialProfileId($publicSocialProfileId)
    {
        $this->publicSocialProfileId = $publicSocialProfileId;
    }

    /**
     * @return null
     */
    public function getSocialSettingId()
    {
        return $this->socialSettingId;
    }

    /**
     * @param null $socialSettingId
     */
    public function setSocialSettingId($socialSettingId)
    {
        $this->socialSettingId = $socialSettingId;
    }

    /**
     * @return null
     */
    public function getSocialProfileName()
    {
        return $this->socialProfileName;
    }

    /**
     * @param null $socialProfileName
     */
    public function setSocialProfileName($socialProfileName)
    {
        $this->socialProfileName = $socialProfileName;
    }

    /**
     * @return null
     */
    public function getSocialProfileScreenName()
    {
        return $this->socialProfileScreenName;
    }

    /**
     * @param null $socialProfileScreenName
     */
    public function setSocialProfileScreenName($socialProfileScreenName)
    {
        $this->socialProfileScreenName = $socialProfileScreenName;
    }

    /**
     * @return null
     */
    public function getSocialProfileImage()
    {
        return $this->socialProfileImage;
    }

    /**
     * @param null $socialProfileImage
     */
    public function setSocialProfileImage($socialProfileImage)
    {
        $this->socialProfileImage = $socialProfileImage;
    }

    /**
     * @return null
     */
    public function getSocialProfileOauthToken()
    {
        return $this->socialProfileOauthToken;
    }

    /**
     * @param null $socialProfileOauthToken
     */
    public function setSocialProfileOauthToken($socialProfileOauthToken)
    {
        $this->socialProfileOauthToken = $socialProfileOauthToken;
    }

    /**
     * @return null
     */
    public function getSocialProfileOauthTokenSecret()
    {
        return $this->socialProfileOauthTokenSecret;
    }

    /**
     * @param null $socialProfileOauthTokenSecret
     */
    public function setSocialProfileOauthTokenSecret($socialProfileOauthTokenSecret)
    {
        $this->socialProfileOauthTokenSecret = $socialProfileOauthTokenSecret;
    }

    /**
     * @return null
     */
    public function getSocialProfileBackgroundColor()
    {
        return $this->socialProfileBackgroundColor;
    }

    /**
     * @param null $socialProfileBackgroundColor
     */
    public function setSocialProfileBackgroundColor($socialProfileBackgroundColor)
    {
        $this->socialProfileBackgroundColor = $socialProfileBackgroundColor;
    }

    /**
     * @return null
     */
    public function getSocialProfileLinkColor()
    {
        return $this->socialProfileLinkColor;
    }

    /**
     * @param null $socialProfileLinkColor
     */
    public function setSocialProfileLinkColor($socialProfileLinkColor)
    {
        $this->socialProfileLinkColor = $socialProfileLinkColor;
    }

    /**
     * @return null
     */
    public function getSocialProfileTextColor()
    {
        return $this->socialProfileTextColor;
    }

    /**
     * @param null $socialProfileTextColor
     */
    public function setSocialProfileTextColor($socialProfileTextColor)
    {
        $this->socialProfileTextColor = $socialProfileTextColor;
    }
}