<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.27.
 * Time: 10:18
 */

namespace Mogul\Model;

interface IStorable
{
    public function getTable();
    public function getPrimaryKey();
    public function getPrimaryKeyValue();
    public function getPrimaryKeyValueQuoted();
    public function getColumns();
    public function getValues();
    public function setValues(array $values);
    public function isNew();
    public function setDB(DB $db);
    public function getDB();
    public function store();
    public function __toString();
}