<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.27.
 * Time: 12:59
 */

namespace Mogul\Model;

class SocialSetting extends AStorable
{
    public $socialSettingId = NULL;
    public $socialSettingStringId = NULL;
    public $socialSettingName = NULL;
    public $socialSettingActive = NULL;

    /**
     * @return null
     */
    public function getSocialSettingId()
    {
        return $this->socialSettingId;
    }

    /**
     * @param null $socialSettingId
     */
    public function setSocialSettingId($socialSettingId)
    {
        $this->socialSettingId = $socialSettingId;
    }

    /**
     * @return null
     */
    public function getSocialSettingStringId()
    {
        return $this->socialSettingStringId;
    }

    /**
     * @param null $socialSettingStringId
     */
    public function setSocialSettingStringId($socialSettingStringId)
    {
        $this->socialSettingStringId = $socialSettingStringId;
    }

    /**
     * @return null
     */
    public function getSocialSettingName()
    {
        return $this->socialSettingName;
    }

    /**
     * @param null $socialSettingName
     */
    public function setSocialSettingName($socialSettingName)
    {
        $this->socialSettingName = $socialSettingName;
    }

    /**
     * @return null
     */
    public function getSocialSettingActive()
    {
        return $this->socialSettingActive;
    }

    /**
     * @param null $socialSettingActive
     */
    public function setSocialSettingActive($socialSettingActive)
    {
        $this->socialSettingActive = $socialSettingActive;
    }
}