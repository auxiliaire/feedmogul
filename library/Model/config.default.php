<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 17:50
 *
 * SAMPLE CONFIGURATION FILE
 * A config.php file must be created in any working environment which must be ignored by version tracking.
 *
 */

define("DB_DSN", 'mysql:dbname=sampledb;host=127.0.0.1');
define("DB_USER", "sampleuser");
define("DB_PASS", "samplepass");