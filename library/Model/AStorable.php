<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.27.
 * Time: 10:19
 */

namespace Mogul\Model;

abstract class AStorable implements IStorable
{
    /**
     * @var null|DB
     */
    protected $_db = NULL;

    public function __construct($db = NULL)
    {
        if (!$db) {
            $db = DB::getInstance();
        }
        $this->setDB($db);
    }

    public function getTable()
    {
        $class = get_class($this);
        // Chop namespaces:
        $a = explode('\\' , $class);
        $table = lcfirst(array_pop($a));
        return $table;
    }

    public function getPrimaryKey()
    {
        return $this->getTable() . "Id";
    }

    public function getPrimaryKeyValue()
    {
        $getter = "get" . ucfirst($this->getPrimaryKey());
        return $this->$getter();
    }

    public function getPrimaryKeyValueQuoted()
    {
        return $this->getDB()->getConnection()->quote($this->getPrimaryKeyValue());
    }

    public function getColumns()
    {
        return array_keys(get_object_vars($this));
    }

    public function getValues()
    {
        $vars = get_object_vars($this);
        $values = array();
        foreach ($vars as $key => $value) {
            if (substr($key, 0, 1) != '_') {
                $values[$key] = $value;
            }
        }
        return $values;
    }

    public function setValues(array $values)
    {
        foreach ($values as $field => $value) {
            $this->$field = $value;
        }
    }

    public function isNew() {
        return (NULL === $this->getPrimaryKeyValue());
    }

    public function store()
    {
        $q = NULL;
        if ($this->isNew()) {
            $q = "INSERT INTO " . $this->getTable();
        } else {
            $q = "UPDATE " . $this->getTable();
        }
        $q .= " SET $this";
        if (!$this->isNew()) {
            $q .= " WHERE " . $this->getPrimaryKey() . " = " . $this->getPrimaryKeyValueQuoted();
        }
        $numRows = $this->getDB()->getConnection()->exec($q);
        // Updating primaryId:
        $setter = "set" . ucfirst($this->getPrimaryKey());
        $this->$setter($this->getDB()->getConnection()->lastInsertId());
        return $numRows;
    }

    public function __toString()
    {
        $a = array();
        foreach ($this->getValues() as $key => $value) {
            $a[] = "$key = " . $this->getDB()->getConnection()->quote($value);
        }
        return implode(", ", $a);
    }

    public function setDB(DB $db) {
        $this->_db = $db;
    }

    public function getDB() {
        return $this->_db;
    }
}