<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 17:49
 *
 * Normally I would use a full featured database abstraction layer like Doctrine2.
 */

namespace Mogul\Model;

use Mogul\Model\DBException;

class DB
{
    protected static $_instance = NULL;
    protected $_numRowsAffected = NULL;

    /**
     * @var null|\PDO
     */
    protected $_dbh = NULL;

    public static function getInstance()
    {
        if (NULL === self::$_instance) {
            self::$_instance = new DB();
        }
        return self::$_instance;
    }

    protected function __construct()
    {
        try {
            $this->_dbh = new \PDO(DB_DSN, DB_USER, DB_PASS);
        } catch (\PDOException $e) {
            throw new DBException($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->_dbh;
    }

    public function store(IStorable $storable) {
        $this->_setNumRowsAffected(
            $storable->store()
        );
    }

    /**
     * @return null
     */
    public function getNumRowsAffected()
    {
        return $this->_numRowsAffected;
    }

    /**
     * @param null $numRowsAffected
     */
    protected function _setNumRowsAffected($numRowsAffected)
    {
        $this->_numRowsAffected = $numRowsAffected;
    }

    /**
     * Finds a model entity in the DB by primary id,
     * returns empty model if not found.
     *
     * @param $model
     * @param $id
     * @return IStorable|mixed
     */
    public function find($model, $id)
    {
        /* @var $model IStorable */
        $model = "\Mogul\Model\\" . $model;
        $model = new $model($this);
        $stmt = $this->getConnection()->prepare("
            SELECT d.* FROM " . $model->getTable() . " d
            WHERE d." . $model->getPrimaryKey() . " = :id
                LIMIT 1
        ");
        $stmt->execute(array("id" => $id));
        $result = $stmt->fetchObject(get_class($model));
        if ($result) {
            return $result;
        } else {
            return $model;
        }
    }

    /**
     * Finds a model entity in the DB by field value,
     * returns empty model if not found.
     *
     * @param $model
     * @param $field
     * @param $value
     * @return IStorable|mixed
     */
    public function findOneBy($model, $field, $value)
    {
        /* @var $model IStorable */
        $model = "\Mogul\Model\\" . $model;
        $model = new $model($this);
        $stmt = $this->getConnection()->prepare("
            SELECT d.* FROM " . $model->getTable() . " d
            WHERE d." . $field . " = :val
                LIMIT 1
        ");
        $stmt->execute(array("val" => $value));
        $result = $stmt->fetchObject(get_class($model));
        if ($result) {
            return $result;
        } else {
            $model->$field = $value;
            return $model;
        }
    }
}