<?php
/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.27.
 * Time: 10:16
 */

namespace Mogul\Model;

class SocialStatus extends AStorable
{
    public $socialStatusId = NULL;
    public $publicSocialStatusId = NULL;
    public $socialProfileId = NULL;
    public $socialStatusText = NULL;
    public $socialStatusCreatedAt = NULL;

    /**
     * @return null
     */
    public function getPublicSocialStatusId()
    {
        return $this->publicSocialStatusId;
    }

    /**
     * @param null $publicSocialStatusId
     */
    public function setPublicSocialStatusId($publicSocialStatusId)
    {
        $this->publicSocialStatusId = $publicSocialStatusId;
    }

    /**
     * @return null
     */
    public function getSocialProfileId()
    {
        return $this->socialProfileId;
    }

    /**
     * @param null $socialProfileId
     */
    public function setSocialProfileId($socialProfileId)
    {
        $this->socialProfileId = $socialProfileId;
    }

    /**
     * @return null
     */
    public function getSocialStatusText()
    {
        return $this->socialStatusText;
    }

    /**
     * @param null $socialStatusText
     */
    public function setSocialStatusText($socialStatusText)
    {
        $this->socialStatusText = $socialStatusText;
    }

    /**
     * @return null
     */
    public function getSocialStatusCreatedAt()
    {
        return $this->socialStatusCreatedAt;
    }

    /**
     * @param null $socialStatusCreatedAt
     */
    public function setSocialStatusCreatedAt($socialStatusCreatedAt)
    {
        $this->socialStatusCreatedAt = $socialStatusCreatedAt;
    }

    /**
     * @return null
     */
    public function getSocialStatusId()
    {
        return $this->socialStatusId;
    }

    /**
     * @param null $socialStatusId
     */
    public function setSocialStatusId($socialStatusId)
    {
        $this->socialStatusId = $socialStatusId;
    }

}