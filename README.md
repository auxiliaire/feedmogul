# README #

This README documents the steps necessary to get your application up and running.

### What is this all about? ###

A small PHP demonstration

### Features ###

#### App Features ####

* Pull a preconfigured user's feed from Twitter
* Store feed in a database
* Render feed using the database
* Use the colors provided by the social API
* Interact with the social API:
  * Retweet
  * Reply
  * Favorite tweet

#### Code Features ####

* A minimal but reusable MVC micro framework
* OOP classes, namespaces
* Autoloading
* Web and CLI usage
* Database abstraction
* Social interaction
* Composer
* 3rd party Twitter library
* Basic error handling

### How do I get set up? ###

* Run composer to add third party twitter library
* Add library/Model/config.php using config.default.php as default
* Source migration.sql dump file from library/Model/migration.sql.zip
* Set webserver document root to public
* Add cron: "php public/index.php cron"
* No further configuration needed

### How to use this demo? ###

* For initialization run cron or alternatively type /cron to web browser
* / for list retrieved from database
* /invalidpage for basic error handling
* On list click Retweet, Reply, Favorite links on each statuses to interact
