<?php

/**
 * Created by PhpStorm.
 * User: Viktor Daróczi
 * Date: 2015.09.26.
 * Time: 16:58
 *
 * In production I would use a full featured framework instead of this,
 * but for demonstration purposes I tried to keep the number of third
 * party libraries on the minimum.
 */
// Possible place to override constants with include.

defined("TWITTER_USER") || define("TWITTER_USER", "viktordaroczi");
defined("LIBRARY_PATH") || define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/../library'));
defined("APPLICATION_PATH") || define("APPLICATION_PATH", realpath(dirname(__FILE__) . '/../application'));
defined("CONTROLLER_PATH") || define("CONTROLLER_PATH", APPLICATION_PATH . "/controller");
defined("VIEW_PATH") || define("VIEW_PATH", APPLICATION_PATH . "/view");
// Third party twitter library OK? Not to reinvent the wheel...
defined("VENDOR_PATH") || define("VENDOR_PATH", realpath(LIBRARY_PATH . "/../vendor"));

require(LIBRARY_PATH . "/Model/config.php");
require(VENDOR_PATH . "/autoload.php");

$bootstrap = new Bootstrap();
$bootstrap->run();

class Bootstrap
{
    public function run()
    {
        $this->_initAutoloader();
        if ("cli" == php_sapi_name()) {
            // CLI USAGE
            $argv = $_SERVER['argv'];
            try {
                switch ($argv[1]) {
                    case "-h":
                    case "--help":
                        echo "Mogul feed reader cron." . PHP_EOL . "Usage: php index.php cron" . PHP_EOL;
                        break;
                    case "cron":
                        include(CONTROLLER_PATH . "/CronController.php");
                        $controller = new CronController();
                        $controller->fetchAction();
                        break;
                    default:
                        throw new \Exception("Unknown parameter {$argv[1]}.");
                }
            } catch (\Exception $e) {
                echo "Error running {$argv[0]}." . PHP_EOL . $e->getMessage() . PHP_EOL;
            }
        } else {
            // DUMMY ROUTER
            $requestUri = $_SERVER['REQUEST_URI'];
            $uriParts = explode('/', $requestUri);
            $uriParts = explode('?', $uriParts[1]);
            try {
                session_start();
                header('Content-Type: text/html; charset=utf-8');
                switch ($uriParts[0]) {
                    case "":
                        $uriParts[0] = "index";
                    case "index":
                    case "favorite":
                    case "retweet":
                    case "reply":
                        include(CONTROLLER_PATH . "/ListController.php");
                        $controller = new ListController();
                        $method = $uriParts[0];
                        $content = $controller->$method();
                        break;
                    case "cron": // For web demo
                        include(CONTROLLER_PATH . "/CronController.php");
                        $controller = new CronController();
                        $controller->fetchAction();
                    default:
                        include(CONTROLLER_PATH . "/ErrorController.php");
                        $controller = new ErrorController();
                        $controller->view->requestedPage = '/' . $uriParts[0];
                        $content = $controller->e404();
                }
            } catch (\Exception $e) {
                include(CONTROLLER_PATH . "/ErrorController.php");
                $controller = new ErrorController();
                $controller->view->message = $e->getMessage();
                switch (get_class($e)) {
                    case "Mogul\Model\DBException":
                        $content = $controller->dbError();
                        break;
                    default:
                        $content = $controller->generalError();
                }
            }
            // LAYOUT RENDERING:
            $view = $controller->view;
            $view->setScript(VIEW_PATH . "/layout.phtml");
            $view->content = $content;

            echo $view->render();

        }
    }

    /**
     * Dummy autoloader
     * Namespace root corresponds to library
     */
    protected function _initAutoloader()
    {
        spl_autoload_register(function ($class) {
            $pos = strpos($class, '\\');
            if ($pos !== FALSE) {
                if ($pos != 0) {
                    $class = __NAMESPACE__ . '\\' . $class;
                }
                $class = str_replace("Mogul\\", '', $class);
                $class = str_replace('\\', '/', $class);
            }
            require_once LIBRARY_PATH . $class . ".php";
        });
    }
}